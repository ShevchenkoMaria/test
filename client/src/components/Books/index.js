import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import {createBook} from '../../actions'

class Books extends Component {
    constructor(props){
        super(props);
        this.state = {
            books: props.books,
            showCreate: false,
            newName: '',
            newAuthor: '',
            newPrice: 0,
            newRating: 0
        };
    }

    componentWillMount() {
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.books !== this.props.books) {
            this.setState({books: nextProps.books})
        }
    }

    _onCreateNewBook(){
        let data = {
            name: this.state.newName,
            author: this.state.newAuthor,
            price: this.state.newPrice,
            rating: this.state.newRating
        };
        this.props.createBook(data);
        this.setState({showCreate: false})
    }

    _onEditName(event){
        this.setState({newName: event.target.value});
    }

    _onEditAuthor(event){
        this.setState({newAuthor: event.target.value});
    }
    _onEditPrice(event){
        if(event.target.value >=0)
            this.setState({newPrice: event.target.value});
    }

    _onEditRating(event){
        if(event.target.value >=0 && event.target.value <= 5)
            this.setState({newRating: event.target.value});
    }
    _onCancelCreate(){
        this.setState({
            newAuthor: '',
            newName: '',
            newPrice: 0,
            newRating: 0,
            showCreate: false
        });
    }

    _onToggleCreate(){
        this.setState({showCreate: !this.state.showCreate})
    }


    renderCreateNew(){
        if(this.state.showCreate){
            return <div className="popup create-popup">
                <div className="container-fluid">
                    <div className="row padding-5">
                        <div className="col-xs-4"><h3>Name</h3></div>
                        <div  className="col-xs-8">
                            <input className="form-control" value={this.state.newName} onChange={this._onEditName.bind(this)} />
                        </div>

                    </div>
                    <div className="row padding-5">
                        <div className="col-xs-4"><h3>Author</h3></div>
                        <div  className="col-xs-8">
                            <input className="form-control" value={this.state.newAuthor} onChange={this._onEditAuthor.bind(this)} />
                        </div>
                    </div>
                    <div className="row padding-5">
                        <div className="col-xs-4"><h3>Price</h3></div>
                        <div  className="col-xs-8">
                            <input className="form-control" value={this.state.newPrice} onChange={this._onEditPrice.bind(this)} />
                        </div>

                    </div>
                    <div className="row padding-5">
                        <div className="col-xs-4"><h3>Rating</h3></div>
                        <div  className="col-xs-8">
                            <input className="form-control" value={this.state.newRating} onChange={this._onEditRating.bind(this)} />
                        </div>
                    </div>
                    <div className="row padding-5">
                        <button type="button" className="btn btn-primary" onClick={this._onCreateNewBook.bind(this)}>Create</button>
                        <button type="button" className="btn btn-default" onClick={this._onCancelCreate.bind(this)}>Cancel</button>
                    </div>
                </div>
            </div>
        }
    }

    renderBooks(){
        let books = this.state.books.map((book) =>
            <div key={book._id} className="row padding-5 relative">
                <div className="col-xs-3"> <Link to={`/books/${book._id}`}>{book.name}</Link></div>
                <div className="col-xs-3">{book.author}</div>
                <div className="col-xs-3">{book.rating}</div>
                <div className="col-xs-3">{book.price}</div>
            </div>
        );
        return <div>{books}</div>
    }


    render(){
        return (
        <div>
            <div className='container-fluid'>
                <div className="row padding-5 relative">
                    <h2 className="left">Books</h2>
                    <button type="button" className="btn btn-primary right" onClick={this._onToggleCreate.bind(this)}>Create New</button>
                    {this.renderCreateNew()}
                </div>
                <div className="row padding-5">
                    <div className="col-xs-3"><h3>Name</h3></div>
                    <div className="col-xs-3"><h3>Author</h3></div>
                    <div className="col-xs-3"><h3>Price</h3></div>
                    <div className="col-xs-3"><h3>Rating</h3></div>
                </div>
                {this.renderBooks()}
            </div>
        </div>
        );
    }
}
Books.propTypes = {
    books: PropTypes.array
};

const mapStateToProps = (state)=>{
    return {
        books: state.books
    };
};

export default connect(mapStateToProps, {createBook})(Books);