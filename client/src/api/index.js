import { Observable } from 'rxjs/Observable';
import { browserHistory } from 'react-router'
import Http from "../utils/http";

function urlBuilder(){
    return  Array.prototype.join.call(arguments, '/')
}

const url = 'http://localhost:3001';
let api_prefix = 'api';
let collections = 'collections';
let books = 'books';

class Api {
    static getAllCollections() {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, collections),
                method: 'get'
            }).then( data =>{
                observer.next(data);
                observer.complete();
            });
        });
    }
    static getCollection(id) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, collections, id),
                method: 'get'
            }).then( data =>{
                observer.next(data);
                observer.complete();
            });
        });
    }
    static createCollection(data) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, collections),
                method: 'post',
                data: data
            }).then( data =>{
                observer.next(data);
                observer.complete();
            });
        });
    }
    static deleteCollection(id) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, collections, id),
                method: 'DELETE'
            }).then(() =>{
                browserHistory['replace']('/collections');
                observer.next();
                observer.complete();
            });
        });
    }
    static editCollection(id, data) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, collections, id),
                method: 'put',
                data: data
            }).then( data =>{
                observer.next(data);
                observer.complete();
            });
        });
    }

    static getAllBooks() {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, books),
                method: 'get'
            }).then( data =>{
                observer.next(data);
                observer.complete();
            });
        });
    }

    static getBook(id) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, books, id),
                method: 'get'
            }).then( data =>{
                observer.next(data);
                observer.complete();
            });
        });
    }
    static createBook(data) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, books),
                method: 'post',
                data: data
            }).then( (data) =>{
                observer.next(data);
                observer.complete();
            });
        });
    }
    static deleteBook(id) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, books, id),
                method: 'DELETE'
            }).then( (data) =>{
                browserHistory['replace']('/books');
                observer.next(data);
                observer.complete();
            });
        });
    }
    static addBook(collectionId, data) {
        return new Observable(observer => {
            Http._axios({//api/collections/:collectionId/books
                url: urlBuilder(url, api_prefix, collections, collectionId, books),
                method: 'POST',
                data: data
            }).then( _data =>{
                observer.next(_data);
                observer.complete();
            });
        });
    }
    static removeBook(collectionId, bookId) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, collections, collectionId, books, bookId),
                method: 'DELETE'
            }).then( data =>{
                observer.next(data);
                observer.complete();
            });
        });
    }

    static editBook(id, data) {
        return new Observable(observer => {
            Http._axios({
                url: urlBuilder(url, api_prefix, books, id),
                method: 'put',
                data: data
            }).then( data =>{
                observer.next(data);
                observer.complete();
            });
        });
    }
}

export default Api;