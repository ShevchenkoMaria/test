import React from 'react';
import { Route } from 'react-router';
import App from '../containers/App';
import NotFound from '../containers/NotFound';
import Books from '../components/Books';
import BookDetails from '../components/Books/Details';
import CollectionsPage from '../components/Collections';
import CollectionDetails from '../components/Collections/Details';

const routes = ()=> {
    return (
        <Route public path="/" component={App}>
            <Route path="/books" component={Books} />
            <Route path="/books/:book_id" component={BookDetails}/>

            <Route path="/collections" component={CollectionsPage} />
            <Route path="/collections/:collection_id" component={CollectionDetails}/>

            <Route path="*" component={NotFound} />
        </Route>
    )};

export default routes;
