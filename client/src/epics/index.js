import 'rxjs';
import { combineEpics } from 'redux-observable';
import Api from '../api';
import * as constants from '../constants';
import * as actions from '../actions';

const getCollections = (action$) => {
    return action$
        .ofType(constants.GET_ALL_COLLECTIONS).switchMap(q => {
            return Api.getAllCollections().map(actions.receiveCollections)
        });
};

const getCollection = (action$) => {
    return action$
        .ofType(constants.GET_COLLECTION).switchMap(q => {
            return Api.getCollection(q.id).map(actions.receiveCollection)
        });
};
const createCollection = (action$) => {
    return action$
        .ofType(constants.CREATE_COLLECTION).switchMap(q => {
            return Api.createCollection(q.data).map(actions.getAllCollections)
        });
};
const deleteCollection = (action$) => {
    return action$
        .ofType(constants.DELETE_COLLECTION).switchMap(q => {
            return Api.deleteCollection(q.id).map(actions.getAllCollections)
        });
};
const editCollection = (action$) => {
    return action$
        .ofType(constants.EDIT_COLLECTION).switchMap(q => {
            return Api.editCollection(q.id, q.data).map(actions.receiveCollection)
        });
};
const addBook = (action$) => {
    return action$
        .ofType(constants.ADD_BOOK).switchMap(q => {
            return Api.addBook(q.collectionId, q.data).map(actions.receiveCollection)
        });
};
const removeBook = (action$) => {
    return action$
        .ofType(constants.REMOVE_BOOK).switchMap(q => {
            return Api.removeBook(q.collectionId, q.bookId).map(actions.receiveCollection)
        });
};
const getBooks = (action$) => {
    return action$
        .ofType(constants.GET_ALL_BOOKS).switchMap(q => {
            return Api.getAllBooks().map(actions.receiveBooks)
        });
};

const getBook = (action$) => {
    return action$
        .ofType(constants.GET_BOOK).switchMap(q => {
            return Api.getBook(q.id).map(actions.receiveBook)
        });
};
const createBook = (action$) => {
    return action$
        .ofType(constants.CREATE_BOOK).switchMap(q => {
            return Api.createBook(q.data).map(actions.getAllBooks)
        });
};
const deleteBook = (action$) => {
    return action$
        .ofType(constants.DELETE_BOOK).switchMap(q => {
            return Api.deleteBook(q.id).map(actions.getAllBooks);
        });
};
const editBook = (action$) => {
    return action$
        .ofType(constants.EDIT_BOOK).switchMap(q => {
            return Api.editBook(q.id, q.data).map(actions.receiveBook)
        });
};

export const rootEpic = combineEpics(
    getCollections,
    getCollection,
    createCollection,
    createBook,
    deleteCollection,
    editCollection,
    getBooks,
    getBook,
    addBook,
    deleteBook,
    removeBook,
    editBook
);




