import * as constants from '../constants'

export function getAllCollections() {
    return {
        type: constants.GET_ALL_COLLECTIONS
    }
}

export function getCollection(id) {
    return {
        type: constants.GET_COLLECTION,
        id: id
    }
}
export function editCollection(id, data) {
    return {
        type: constants.EDIT_COLLECTION,
        id: id,
        data: data
    }
}
export function createCollection(data) {
    return {
        type: constants.CREATE_COLLECTION,
        data: data
    }
}
export function deleteCollection(id) {
    return {
        type: constants.DELETE_COLLECTION,
        id: id
    }
}

export function receiveCollections(collections) {
    return {
        type: constants.RECEIVE_COLLECTIONS,
        collections: collections
    }
}

export function receiveCollection(collection) {
    return {
        type: constants.RECEIVE_COLLECTION,
        collection: collection
    }
}
export function getAllBooks() {
    return {
        type: constants.GET_ALL_BOOKS
    }
}
export function getBook(id) {
    return {
        type: constants.GET_BOOK,
        id: id
    }
}

export function receiveBooks(books) {
    return {
        type: constants.RECEIVE_BOOKS,
        books: books
    }
}
export function receiveBook(book) {
    return {
        type: constants.RECEIVE_BOOK,
        book: book
    }
}

export function deleteBook(id) {
    return {
        type: constants.DELETE_BOOK,
        id: id
    }
}
export function addBook(collectionId, data) {
    return {
        type: constants.ADD_BOOK,
        collectionId: collectionId,
        data: data
    }
}
export function removeBook(collectionId, bookId) {
    return {
        type: constants.REMOVE_BOOK,
        collectionId: collectionId,
        bookId: bookId
    }
}

export function createBook(data) {
    return {
        type: constants.CREATE_BOOK,
        data: data
    }
}
export function editBook(id, data) {
    return {
        type: constants.EDIT_BOOK,
        id: id,
        data: data
    }
}