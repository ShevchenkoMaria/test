import { applyMiddleware, createStore } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import * as actions from '../actions';
import { rootEpic } from '../epics';
import reducer from "../reducers";

const epicMiddleware = createEpicMiddleware(rootEpic);

const store = createStore(
    reducer,
    applyMiddleware(epicMiddleware)
);

store.dispatch(actions.getAllCollections());
store.dispatch(actions.getAllBooks());

export default store;
